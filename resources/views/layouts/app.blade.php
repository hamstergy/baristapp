<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="manifest" href="/manifest.json">
    <link rel="icon" href="/icons/icon_48_48_dpi.png" sizes="48x48" />
    <link rel="icon" href="/icons/icons/icon_72_48_dpi.png" sizes="72x72" />
    <link rel="icon" href="/icons/icons/icon_128_128_dpi.png" sizes="128x128" />
    <link rel="icon" href="/icons/icon_256_128_dpi.png" sizes="256x256" />
    <link rel="icon" href="/icons/icon_512_128_dpi.png" sizes="512x512" />
    <link rel="apple-touch-icon" sizes="180x180" href="/icons/icon-iphone-180.png">
    <link rel="apple-touch-icon" href="/icons/icon-iphone-180.png">
    <title>{{ config('app.name', 'Baristapp') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script>
            if ('serviceWorker' in navigator ) {
              window.addEventListener('load', function() {
                  navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
                      // Registration was successful
                      console.log('ServiceWorker registration successful with scope: ', registration.scope);
                  }, function(err) {
                      // registration failed :(
                      console.log('ServiceWorker registration failed: ', err);
                  });
              });
          }
  </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <a class="dropdown-item" href="{{ route('home') }}">
                            {{ __('Меню') }}
                            </a>
                            <a class="dropdown-item" href="{{ url('users/'.Auth::id()) }}">
                            {{ __('Мои продажи') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            {{ __('Выход') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="pb-4" style="padding-top: 5rem!important;">
            @yield('content')
        </main>
        <nav class="navbar navbar-static-top navbar-dark bg-dark">
            <div class="container">
                <form class="form-inline mx-auto">
                <button style="margin: 0 10px;" onclick="window.location='{{ route('home') }}'" class="btn btn-outline-primary mr-sm-2 @if (Request::is('home')) {{'active'}} @endif" type="button">Меню</button>
                <button style="margin: 0 10px;" onclick="window.location='{{ url('users/'.Auth::id()) }}'" class="btn btn-outline-primary mr-sm-2 @if (Request::is('users*')) {{'active'}} @endif" type="button">Продажи</button>
                </form>
            </div> 
        </nav>
    </div>
</body>
</html>
