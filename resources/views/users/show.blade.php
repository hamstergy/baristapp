@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Мои продажи</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   
                    
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>Продажа</td>
                                <td>Время</td>
                                <td>Цена</td>
                                <td>Доход</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user->products as $product)
                            <tr>
                                <td>{{$product->name}}</td>
                                <td>{{ \Carbon\Carbon::parse($product->orders->created_at)->format('H:i')}}</td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->price * $product->reward * 0.01}}</td>
                            </tr>
                            @endforeach
                            <tr class="table-success">
                                <td colspan=2>Итого</td>
                                <td>{{$todayCash}}</td>
                                <td>{{$todayReward}}</td>
                            </tr>
                        </tbody>
                      </table>
                      

                </div>
            </div>
        </div>
    </div>
</div>


@endsection