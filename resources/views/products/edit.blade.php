@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                <form method="post" action="{{ route('products.update', $product->id) }}">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value={{ $product->name }} />
                    </div>
                    <div class="form-group">
                    <label for="price">Price:</label>
                    <input type="text" class="form-control" name="price" value={{ $product->price }} />
                    </div>
                    <div class="form-group">
                    <label for="size">Size:</label>
                    <input type="text" class="form-control" name="size" value={{ $product->size }} />
                    </div>
                    <div class="form-group">
                        <label for="reward">Reward:</label>
                        <input type="text" class="form-control" name="reward" value={{ $product->reward }} />
                    </div>
                    <button type="submit" class="btn btn-primary">Обновить</button>
                </form>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection