<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="/manifest.json">
        <link rel="icon" href="/icons/icon_48_48_dpi.png" sizes="48x48" />
        <link rel="icon" href="/icons/icons/icon_72_48_dpi.png" sizes="72x72" />
        <link rel="icon" href="/icons/icons/icon_128_128_dpi.png" sizes="128x128" />
        <link rel="icon" href="/icons/icon_256_128_dpi.png" sizes="256x256" />
        <link rel="icon" href="/icons/icon_512_128_dpi.png" sizes="512x512" />
        <link rel="apple-touch-icon" sizes="180x180" href="/icons/icon-iphone-180.png">
        <link rel="apple-touch-icon" href="/icons/icon-iphone-180.png">
        <title>Baristapp</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Styles -->
        <style>

            .full-height {
                height: 80vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 56px;
            }

            /* .links > a {
                color: #636b6f;
                padding: 0 5px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            } */

            .m-b-md {
                margin-bottom: 30px;
            } 
        </style> 
        <script>
                if ('serviceWorker' in navigator ) {
                  window.addEventListener('load', function() {
                      navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
                          // Registration was successful
                          console.log('ServiceWorker registration successful with scope: ', registration.scope);
                      }, function(err) {
                          // registration failed :(
                          console.log('ServiceWorker registration failed: ', err);
                      });
                  });
              }
      </script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            
<div class="container">
            <div class="content">
                <div class="title m-b-md">
                    <h2>Baristapp</h2>
                </div>
                <div>
                    @auth
                        <a href="{{ url('/home') }}"><h5>Меню</h5></a>
                        <a href="{{ url('users/'.Auth::id()) }}"><h5>Продажи</h5></a>
                    @else
                    <div class="col-md-12">
                        <div>
                            <div>
                                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                                    @csrf
                                    
            
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
            
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
            
                                    <div class="form-group row">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Пароль') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
            
                                    <div class="form-group row">
                                        <div class="col-md-7 offset-md-3">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            
                                                <label class="form-check-label" for="remember">
                                                    {{ __('Запомнить меня') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
            
                                    
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Войти') }}
                                            </button>
            
                                            {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a> --}}
                                        
                                </form>
                            </div>
                        </div>
                    </div>
                        {{-- <a href="{{ route('register') }}">Register</a> --}} 
                    @endauth

                </div>
            </div>
        </div>
        </div>
    </body>
</html>
