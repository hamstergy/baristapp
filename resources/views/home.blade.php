@extends('layouts.app')
@section('title', $title.' - Baristapp')
@section('description', $description)
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Меню</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                    @foreach($products as $type)
                        <div class="col-xs-12 col-lg-6" style="line-height: 1.4;">
                            <div class="media pt-3">
                                <div class="media-body pb-3 mb-0 medium lh-125 border-bottom border-gray">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong>{{ $type->name }} </strong>
                                    <span class="d-block text-muted">{{ $type->size }} </span>
                                    
                                    <form action="{{ route('orders.store', [ 'product_id' => $type->id, 'user_id' => Auth::id()])}}" method="post">
                                        @csrf
                                        <button class="btn btn-success" type="submit">{{ $type->price}}</button>
                                    </form>
                                    {{-- <a class="btn btn-success" href="{{ route('orders.store', [$type->id, 1])}}" >{{ $type->price}}</a> --}}
                                </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
