<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    public function products()
    {
        // return $this->hasMany(Product::class,'orders','user_id','product_id');
        // return $this->hasMany(Product::class,);
        return $this->belongsToMany(Product::class,'orders','user_id','product_id')
            ->as('orders')
            ->withTimestamps()
            ->where('orders.created_at', '>=', Carbon::today())
            ->orderBy('orders.created_at', 'asc');
    }

}
