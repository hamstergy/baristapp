<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'price', 'size', 'reward' 
    ];

    public function stocks()
    {
        return $this->belongsToMany(Stock::class)->withPivot('quantity')
    	->withTimestamps();
    }
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class,'orders','product_id','user_id');
    }
    
    
    
}
