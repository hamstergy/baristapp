<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('name', 'ASC')->get();
        $data = [
            'title' => 'Меню на сегодня',
            'pagetitle' => 'Ассортимент',
            'products' => $products,
            'description' => 'Меню на сегодня'
        ];
        return view('home', $data);
    }
}
